//
//  CategoryViewController.swift
//  Todoey
//
//  Created by Faiq on 13/03/2021.
//  Copyright © 2021 App Brewery. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift
import ChameleonFramework

class CategoryViewController: SwipeTableViewController {
    
    var categories: Results<CategoryRealm>?
    
    //for coredata
    //let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //initialize realm
    let realm = try! Realm()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadCatgories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let navBar = navigationController?.navigationBar else {fatalError("Navigation controller does not exits.")}
        navBar.backgroundColor = UIColor.systemTeal
    }
    
    //MARK:- Add New Categories
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add New Category", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Category", style: .default) { (action) in
            let newCategory = CategoryRealm()
            newCategory.name = textField.text!
            newCategory.colour = UIColor.randomFlat().hexValue()
            
            //used in user defaults and coredata
            //self.categories.append(newCategory)
            
            self.save(category: newCategory)
        }
        
        alert.addTextField { (field) in
            field.placeholder = "Add new category"
            textField = field
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if let category = categories?[indexPath.row] {
            cell.textLabel?.text = category.name ?? "No Categories added yet"
            guard let categoryColour = UIColor(hexString: category.colour) else {fatalError()}
            cell.backgroundColor = categoryColour
            cell.textLabel?.textColor = ContrastColorOf(backgroundColor: categoryColour, returnFlat: true)
        }
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! TodoListViewController
        
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedCategory = categories?[indexPath.row]
        }
    }
    
    //MARK:- Realm Functions
    func save(category: CategoryRealm){
        //save a category
        do {
            try realm.write {
                realm.add(category)
            }
        } catch {
            print("Error saving category, \(error)")
        }

        tableView.reloadData()
    }

    func loadCatgories(){
        categories = realm.objects(CategoryRealm.self)
        tableView.reloadData()
    }
    
    //MARK:- Delete Data From Swipe
    override func updateModel(at indexPath: IndexPath){
        //update our data model
        if let categoryForDeletion = self.categories?[indexPath.row] {
            do {
                try self.realm.write{
                    self.realm.delete(categoryForDeletion)
                }
            } catch {
                print("Error deleting category",error)
            }
            //tableView.reloadData()
        }
    }
    
    // MARK: - Core Data function
    /*
    func saveCategories(){
        //save a category
        do {
            try context.save()
        } catch {
            print("Error saving category, \(error)")
        }
        
        tableView.reloadData()
    }

    func loadCatgories(){
        let request: NSFetchRequest<Category> = Category.fetchRequest()
        do {
            categories = try context.fetch(request)
        }catch {
            print("Error loading categories \(error)")
        }
        tableView.reloadData()
    }
 */
    
}

