//
//  ViewController.swift
//  Todoey
//
//  Created by Philipp Muellauer on 02/12/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift
import ChameleonFramework

class TodoListViewController: SwipeTableViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK:- Constants
    //var itemArray = [Item]
    
    var todoItems: Results<ItemRealm>?
    
    var selectedCategory: CategoryRealm? {
        didSet{
            loadItems()
        }
    }
    
    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Items.plist")
    /*
     We are getting into UIApplication class and getting shared singleton obj which corresponds to current app as an obj. Then going into its delegate, then casting it into AppDelegate.
     Now we have access to AppDelegate as obj and can access persistentContainer and its viewContext
     */
    
    //let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //initialize realm
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(dataFilePath)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let colourHex = selectedCategory?.colour {
            //set title
            title = selectedCategory!.name
            
            //set nav bar background color
            guard let navBar = navigationController?.navigationBar else {fatalError("Navigation controller does not exits.")}
            
            if let navBarColour = UIColor(hexString: colourHex) {
                //set nav bar background color
                navBar.backgroundColor = navBarColour
                
                //set nav bar tint color
                navBar.tintColor = ContrastColorOf(backgroundColor: navBarColour, returnFlat: true)
                
                navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: ContrastColorOf(backgroundColor: navBarColour, returnFlat: true)]
                
                //set search bar tint color
                searchBar.barTintColor = navBarColour
            }
        }
        
    }
    
    //MARK:- Add new items
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add New Todoey Item", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            
            if let currentCategory = self.selectedCategory {
                do {
                    try self.realm.write{
                        let newItem = ItemRealm()
                        newItem.title = textField.text!
                        newItem.dateCreated = Date()
                        //newItem.done = false
                        currentCategory.items.append(newItem)
                    }
                }catch {
                    print("Error saving new items, \(error)")
                }
                
            }
            //save new item
            //self.saveItems()
            
            //tableview reload
            self.tableView.reloadData()
        }
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new item"
            textField = alertTextField
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:- TableView DataSource Methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoItems?.count ?? 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if let item = todoItems?[indexPath.row] {
            cell.textLabel?.text = item.title
            
            if let color = UIColor(hexString: selectedCategory!.colour)?.darken(
                byPercentage:
                    //currently on row 5
                    //there's a total of 10 items in todoItems
                    CGFloat(indexPath.row)/CGFloat(todoItems!.count)
            ) {
                cell.backgroundColor = color
                cell.textLabel?.textColor = ContrastColorOf(backgroundColor: color, returnFlat: true)
            }
            //Ternary operator =>
            // value = condition ? valueIfTrue : valueIfFalse
            cell.accessoryType = item.done == true ? .checkmark : .none
            
        } else {
            cell.textLabel?.text = "No Items Added"
        }

        return cell 
    }
    
    //MARK:- TableView Delegate Methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let item = todoItems? [indexPath.row] {
            do {
                try realm.write {
                    //if want to delete
                    //realm.delete(item)
                    
                    //to toggle checkmark
                    item.done = !item.done
                }
            } catch {
                print("Error saving done status", error)
            }
        }
        
        tableView.reloadData()
        
        //it sets the done property of current item to its opposite
        //todoItems?[indexPath.row].done = !todoItems[indexPath.row].done
        
        //if want to to delete item on tap CRUD
        //context.delete(itemArray[indexPath.row])
        //itemArray.remove(at: indexPath.row)
        
        //save check property of item in plist
        //saveItems()
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- Realm Methods
    func loadItems(){
        todoItems = selectedCategory?.items.sorted(byKeyPath: "title", ascending: true)
        tableView.reloadData()
    }
    
    override func updateModel(at indexPath: IndexPath) {
        if let item = todoItems?[indexPath.row] {
            do {
                try realm.write{
                    realm.delete(item)
                }
            } catch {
                
            }
        }
    }
    
    //MARK:- Core Data Methods
    /*
    func saveItems() {
        //save an item
        do {
            try context.save()
        } catch {
            print("Error saving context, \(error)")
        }
        
        tableView.reloadData()
    }
    
    func loadItems(with request: NSFetchRequest<Item> = Item.fetchRequest(), predicate: NSPredicate? = nil){
        
        let categoryPredicate = NSPredicate(format: "parentCategory.name MATCHES %@", selectedCategory!.name!)
        
        if let additionalPredicate = predicate {
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [categoryPredicate, additionalPredicate])
        } else {
            request.predicate = categoryPredicate
        }

        do{
            itemArray = try context.fetch(request)
        } catch {
            print("Error fetching data", error)
        }
        tableView.reloadData()
    }
 */
    
}

//MARK:- SearchBar delegate Methods
extension TodoListViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        //filtering item and sorting it in asc order
        //1-same as in coredata
        //todoItems = todoItems?.filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "title", ascending: true)
        
        //2 - using datecreated
        todoItems = todoItems?.filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "dateCreated", ascending: true)
        
        /*
         ///CORE DATA
        let request: NSFetchRequest<Item> = Item.fetchRequest()
         
        //query for the item where title of item Contains search bar text
        let predicate = NSPredicate(format: "title CONTAINS[cd] %@", searchBar.text!)
        
        //sort descriptor expects array
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        //calling req func
        //loadItems(with: request, predicate: predicate)
        
        tableView.reloadData()
 */
    }
    
    //only trigger when the text is changed
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadItems()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}
