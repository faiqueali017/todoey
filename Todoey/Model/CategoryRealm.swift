//
//  Category.swift
//  Todoey
//
//  Created by Faiq on 14/03/2021.
//  Copyright © 2021 App Brewery. All rights reserved.
//

import Foundation
import RealmSwift

class CategoryRealm: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var colour: String = ""
    
    //relation btw the tables
    let items = List<ItemRealm>()
    
}
