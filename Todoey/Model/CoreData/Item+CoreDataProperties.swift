//
//  Item+CoreDataProperties.swift
//  Todoey
//
//  Created by Faiq on 13/03/2021.
//  Copyright © 2021 App Brewery. All rights reserved.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var title: String?
    @NSManaged public var done: Bool
    @NSManaged public var parentCategory: Category?

}

extension Item : Identifiable {

}
