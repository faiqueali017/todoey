//
//  Item.swift
//  Todoey
//
//  Created by Faiq on 14/03/2021.
//  Copyright © 2021 App Brewery. All rights reserved.
//

import Foundation
import RealmSwift

class ItemRealm: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var done: Bool = false
    @objc dynamic var dateCreated: Date?
    //for relation
    var parentCategory = LinkingObjects(fromType: CategoryRealm.self, property: "items")
}
